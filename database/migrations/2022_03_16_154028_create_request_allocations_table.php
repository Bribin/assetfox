<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_allocations', function (Blueprint $table) {

            $table->id();
            $table->bigInteger('request_id')->unsigned()->index();
            $table->BigInteger('asset_id')->unsigned()->index();
            $table->foreign('request_id')->references('id')->on('asset_requests');
            $table->foreign('asset_id')->references('id')->on('assets');
            $table->string('delivery_type');
            $table->string('remarks');
            $table->string('status');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_allocations');
    }
}
