
@extends('layouts.user-dashboard')
@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title ?? '' }}</h3>
                <div class="nk-block-des text-soft">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>
            </div><!-- .nk-block-head-content -->

        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block nk-block-lg">

        <div class="row g-gs">

            <div class="col-lg-8">

                <div class="card card-bordered h-100">
                    <div class="card-inner">

                        {!! Form::model('' , array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
                        {{ Form::hidden('asset_id', $assetID) }}
                        <div class="form-group">
                            <label class="form-label">Reason for Return <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::textarea('note',null, ['class' => 'form-control ', 'placeholder'=>'Enter Reason for Return', 'required' =>'required']) !!}

                            </div>

                        </div>
                        <div class="form-group">

                            <button type="submit" class=" float-right btn btn-lg btn-primary">Save</button>
                        </div>
                        {!! Form::close() !!}
                        @if ($mode  === 'EDIT')

                            <form action="{{url('dashboard/calendars/'.$asset->id.'/')}}" method="post">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure?')"   class="btn btn-link" type="submit">Delete Category</button>
                            </form>
                        @else

                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div><!-- .nk-block -->



@endsection
