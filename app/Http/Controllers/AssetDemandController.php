<?php

namespace App\Http\Controllers;

use App\Models\AssetDemand;
use Illuminate\Http\Request;

class AssetDemandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AssetDemand  $assetDemand
     * @return \Illuminate\Http\Response
     */
    public function show(AssetDemand $assetDemand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AssetDemand  $assetDemand
     * @return \Illuminate\Http\Response
     */
    public function edit(AssetDemand $assetDemand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AssetDemand  $assetDemand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssetDemand $assetDemand)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AssetDemand  $assetDemand
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssetDemand $assetDemand)
    {
        //
    }
}
