
@extends('layouts.dashboard')
@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title  }}</h3>
                <div class="nk-block-des text-soft">
                    <p>You have total {{ $tickets->count() }} Tickets.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">


                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="nk-tb-list is-separate mb-3">
            <div class="nk-tb-item nk-tb-head">


                <div class="nk-tb-col tb-col-mb"><span class="sub-text">title</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Notes</span></div>
                <!-- <div class="nk-tb-col tb-col-md"><span class="sub-text">Status</span></div> -->
            </div><!-- .nk-tb-item -->
            @if(count($tickets ) > 0)
                @foreach($tickets as $ticket)


                    <div class="nk-tb-item">
                        <div class="nk-tb-col tb-col-mb">
                            <span >{{ $ticket->title }} </span>
                        </div>
                        <div class="nk-tb-col">
                            <span >{{ $ticket->note }} </span>
                        </div>


                        <!-- <div class="nk-tb-col tb-col-mb">
                            <span >{{ $ticket->status }} </span>
                        </div> -->
                        <div class="nk-tb-col tb-col-mb">
                            <span > <a class="btn btn-sm btn-outline-dark" href="{{ url('dashboard/tickets/'.$ticket->id.'/reply ') }}"> Reply </a> </span>
                        </div>


                    </div><!-- .nk-tb-item -->

                @endforeach
            @else

            @endif


        </div><!-- .nk-tb-list -->
        <div class="card">
            <div class="card-inner">
                <div class="nk-block-between-md g-3">
                    {{ $tickets->links() }}

                </div><!-- .nk-block-between -->
            </div><!-- .card-inner -->
        </div><!-- .card -->
    </div>


@endsection
