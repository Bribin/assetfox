<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

class RequestAllocation extends Model
{
    use RevisionableTrait;
    use HasFactory;

    protected $fillable = ['request_id', 'asset_id'];

    protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been ma
}
