
@extends('layouts.user-dashboard')
@section('content')

    <div class="container wide-sm">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-content-wrap">
                    <div class="nk-block-head nk-block-head-lg wide-sm m-auto text-center">
                        <!-- <div class="nk-block-head-sub"><a href="support.html" class="text-soft"><span>Contact</span></a></div> -->
                        <div class="nk-block-head-content">
                            <h2 class="nk-block-title fw-normal">How can we help?</h2>
                            <div class="nk-block-des">
                                <p>You can find all of the questions and answers abour secure your account</p>
                            </div>
                        </div>
                    </div><!-- .nk-block-head -->
                    <div class="nk-block mb-3">
                        <div class="card card-bordered">
                            <div class="card-inner">

                                    <div class="row g-4">
                                        <div class="col-lg-12">
                                            {!! Form::model($ticket, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
                                            <div class="form-group">
                                                <label class="form-label" >Request Category <span>*</span></label>
                                                <div class="form-control-wrap">
                                                    {!! Form::select('category',[''=>'','0'=>'General','1'=>'Technical'] ,null, ['data-parsley-errors-container' => '#status-errors','data-placeholder' => 'Select Type','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                                    <div id="status-errors"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" >Priority <span>*</span></label>
                                                <div class="form-control-wrap">
                                                    {!! Form::select('priority',[''=>'','0'=>'Normal','1'=>'Important','2'=>'High Priority'] ,null, ['data-parsley-errors-container' => '#status-errors','data-placeholder' => 'Select Type','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                                    <div id="status-errors"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" >Describe the problem you have <span>*</span></label>
                                                <div class="form-control-wrap">
                                                    {!! Form::text('title',null, ['class' => 'form-control ', 'placeholder'=>'Enter Title ', 'required' =>'required']) !!}
                                                    <div id="status-errors"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" >Give us the details <span>*</span></label>
                                                <p class="text-soft">If you’re experiencing a personal crisis, are unable to cope and need support send message. We will always try to respond to texters as quickly as possible.</p>
                                                <div class="form-control-wrap">
                                                    {!! Form::textarea('note',null, ['class' => 'form-control ', 'placeholder'=>'Enter Message', 'required' =>'required']) !!}
                                                    <div id="status-errors"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">

                                                <button type="submit" class=" float-right btn btn-lg btn-primary">Save</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>

                                    </div>

                            </div><!-- .card-inner -->
                        </div><!-- .card -->
                    </div><!-- .nk-block -->
                </div>
            </div>
        </div>
    </div>



@endsection
