
@extends('layouts.user-dashboard')
@section('content')


    <h3>Product 2: MAC-1200</h3>
    @php
        $generatorPNG = new Picqer\Barcode\BarcodeGeneratorPNG();
    @endphp
    <img src="data:image/png;base64,{{ base64_encode($generatorPNG->getBarcode('SlNC1hVpAoJlEW8K9jJR', $generatorPNG::TYPE_CODE_128)) }}">


@endsection
