<?php

namespace App\Http\Controllers;

use App\Models\Ticket_messages;
use Illuminate\Http\Request;

class TicketMessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ticket_messages  $ticket_messages
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket_messages $ticket_messages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ticket_messages  $ticket_messages
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket_messages $ticket_messages)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ticket_messages  $ticket_messages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket_messages $ticket_messages)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ticket_messages  $ticket_messages
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket_messages $ticket_messages)
    {
        //
    }
}
