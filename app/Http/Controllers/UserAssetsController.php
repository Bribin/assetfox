<?php

namespace App\Http\Controllers;

use App\Models\UserAssets;
use Illuminate\Http\Request;

class UserAssetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserAssets  $userAssets
     * @return \Illuminate\Http\Response
     */
    public function show(UserAssets $userAssets)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserAssets  $userAssets
     * @return \Illuminate\Http\Response
     */
    public function edit(UserAssets $userAssets)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserAssets  $userAssets
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserAssets $userAssets)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserAssets  $userAssets
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserAssets $userAssets)
    {
        //
    }
}
