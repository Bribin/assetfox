<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Venturecraft\Revisionable\RevisionableTrait;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    use RevisionableTrait;
    protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been ma
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public  function getLevel(){

        $status = $this->status;
        switch ($status) {
            case "0":
                echo '<span class="badge badge-outline-primary">UNDER VERIFICATION</span>';
                break;
            case "1":
                echo '<span class="badge badge-success">APPROVED</span>';
                break;

        }


    }


    public  function getRole(){

        $role= $this->role;
        switch ($role) {
            case "0":
                echo '<span class="badge badge-outline-primary">EMPLOYEE</span>';
                break;
            case "1":
                echo '<span class="badge badge-outline-primary">SYSTEM ADMINISTRATOR</span>';
                break;
            case "2":
                echo '<span class="badge badge-outline-primary">ADMINISTRATOR</span>';
                break;

        }


    }



    public function AssetRequests()
    {
        return $this->hasMany(AssetRequest::class,'user_id');
    }
}
