@extends('layouts.dashboard')
@section('content')

<div class="nk-block-head nk-block-head-sm">
    <div class="nk-block-between">
        <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title"></h3>
            <div class="nk-block-des text-soft">
                <p></p>
            </div>
        </div><!-- .nk-block-head-content -->
        <div class="nk-block-head-content">
            <div class="toggle-wrap nk-block-tools-toggle">
                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                <div class="toggle-expand-content" data-content="more-options">
                    <ul class="nk-block-tools g-3">


                    </ul>
                </div>
            </div>
        </div><!-- .nk-block-head-content -->
    </div><!-- .nk-block-between -->
</div><!-- .nk-block-head -->
<div class="nk-block">
    <div class="nk-tb-list is-separate mb-3">
        <div class="nk-tb-item nk-tb-head">

            <div class="nk-tb-col"><span class="sub-text">Logs</span></div>

        </div>


        <!-- .nk-tb-item -->
        @if(count($systemLogs ) > 0)
            @foreach($systemLogs as $history)



                <div class="nk-tb-item">
                    @if($history->key == 'created_at' && !$history->old_value)
                        <div class="nk-tb-col tb-col-mb">
                            <span class="tb-lead">{{ ucfirst($history->userResponsible()->name ?? null ) }} created a {{ $history->revisionable_type  }}  at  {{ $history->created_at }} </span>
                        </div>
                    @else

                        <div class="nk-tb-col">
                            <span class="tb-lead"> {{ ucfirst($history->userResponsible()->name ?? null) }} updated {{ $history->fieldName() }} at {{ $history->created_at }}  </span>

                        </div>
                    @endif

                </div><!-- .nk-tb-item -->

            @endforeach
        @else

        @endif


    </div><!-- .nk-tb-list -->
    <div class="card">
        <div class="card-inner">
            <div class="nk-block-between-md g-3">
                {{ $systemLogs->links() }}

            </div><!-- .nk-block-between -->
        </div><!-- .card-inner -->
    </div><!-- .card -->
</div>
@endsection
