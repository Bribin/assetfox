
@extends('layouts.user-dashboard')
@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title  }}</h3>
                <div class="nk-block-des text-soft">
                    <p>You have total {{ $assetRequests->count() }} Asset Requests.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">

                            <li class="nk-block-tools-opt">
                                <a href="#" class="btn btn-icon btn-primary d-md-none"><em class="icon ni ni-plus"></em></a>
                                <a href="{{ url('/request-assets') }}" class="btn btn-primary d-none d-md-inline-flex"><em class="icon ni ni-plus"></em><span>Add New Request </span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="nk-tb-list is-separate mb-3">
            <div class="nk-tb-item nk-tb-head">
                <div class="nk-tb-col tb-col-md"><span class="sub-text">Request Created At</span></div>
                <div class="nk-tb-col"><span class="sub-text">Request ID</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Asset Type</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Reason</span></div>
                <div class="nk-tb-col tb-col-md"><span class="sub-text">Note</span></div>
                <div class="nk-tb-col tb-col-md"><span class="sub-text">Status</span></div>

            </div><!-- .nk-tb-item -->
            @if(count($assetRequests) > 0)
                @foreach($assetRequests as $assetRequest)
                    <div class="nk-tb-item">
                        <div class="nk-tb-col tb-col-mb">
                            <span >{{ $assetRequest->created_at }} </span>
                        </div>
                        <div class="nk-tb-col tb-col-mb">
                            <span >{{ $assetRequest->id }} </span>
                        </div>
                        <div class="nk-tb-col tb-col-mb">
                            <span >{{ $assetRequest->requestType() }} </span>
                        </div>

                        <div class="nk-tb-col tb-col-mb">
                            <span >{{ $assetRequest->requestReason() }} </span>
                        </div>

                        <div class="nk-tb-col tb-col-mb">
                            <span >{{ $assetRequest->note }} </span>
                        </div>
                        <div class="nk-tb-col tb-col-lg">
                            <span class="tb-lead">   {{ $assetRequest->status() }} </span>
                        </div>
                        <div class="nk-tb-col tb-col-lg">

                            @if( $assetRequest->status == 3)
                                <a href="{{ url('/manage-assets/'.$assetRequest->id.'/ConfirmAssetDelivery') }}" class="btn btn-sm btn-outline-dark">Confirm Asset Delivery</a>
                            @endif
                        </div>



                    </div><!-- .nk-tb-item -->


                        @if($assetRequest->status == 4)

                            @php
                               $assets = DB::table('request_allocations')->where('request_id', '=',$assetRequest->id )->pluck('asset_id');
                            @endphp

                                @foreach($assets as $assetID)

                            <div class="nk-tb-item">
                                <div class="nk-tb-col tb-col-mb">
                                    <span >{{ DB::table('assets')->where('id', '=', $assetID )->value('name') }} </span>
                                </div>
                                <div class="nk-tb-col tb-col-mb">


                                    @if( DB::table('return_requests')->where('asset_id', '=', $assetID)->count() ==! 0 )
                                          <p>Your Asset Return Request is {{ DB::table('return_requests')->where('asset_id', '=', $assetID)->value('status') }}. You will get a call from our delivery partner for Asset PICKUP</p>
                                    @else
                                        <a href="{{ url('manage-assets/'.$assetID.'/ReturnRequest') }}">Request Asset Return </a>
                                    @endif
                                </div>
                                <div class="nk-tb-col tb-col-lg">


                                </div>



                            </div><!-- .nk-tb-item -->

                        @endforeach
                        @endif

                @endforeach
            @else

            @endif






        </div><!-- .nk-tb-list -->
        <div class="card">
            <div class="card-inner">
                <div class="nk-block-between-md g-3">


                </div><!-- .nk-block-between -->
            </div><!-- .card-inner -->
        </div><!-- .card -->
    </div>


@endsection
