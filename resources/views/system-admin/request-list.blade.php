
@extends('layouts.user-dashboard')
@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title  }}</h3>
                <div class="nk-block-des text-soft">
                    <p>You have total {{ $requests->count() }} Asset Requets.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">

            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="nk-tb-list is-separate mb-3">
            <div class="nk-tb-item nk-tb-head">

                <div class="nk-tb-col"><span class="sub-text">Requested By</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Asset Type</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Special Note</span></div>
                <div class="nk-tb-col tb-col-md"><span class="sub-text">Status</span></div>
                <div class="nk-tb-col tb-col-md"><span class="sub-text">Actions</span></div>
            </div><!-- .nk-tb-item -->
            @if(count($requests ) > 0)
                @foreach($requests as $request)
                    <div class="nk-tb-item">
                        <div class="nk-tb-col tb-col-mb">
                            <span >{{ $request->user->name }} </span>
                        </div>

                        <div class="nk-tb-col">
                            <span >   {{ $request->requestType() }} </span>
                        </div>

                        <div class="nk-tb-col tb-col-md">
                            <span class="tb-lead">   {{  $request->note  }} </span>
                        </div>

                        <div class="nk-tb-col tb-col-md">
                            <span class="tb-lead">   {{$request->status()  }} </span>
                        </div>

                        <div class="nk-tb-col tb-col-lg">
                         <a href="{{ url('system-admin/view-request/'.$request->id ) }}" class="btn btn-primary"> Manage Request</a>

                        </div>



                    </div><!-- .nk-tb-item -->

                @endforeach
            @else

            @endif


        </div><!-- .nk-tb-list -->
        <div class="card">
            <div class="card-inner">
                <div class="nk-block-between-md g-3">
                    {{ $requests->links() }}

                </div><!-- .nk-block-between -->
            </div><!-- .card-inner -->
        </div><!-- .card -->
    </div>


@endsection
