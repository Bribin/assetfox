<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Auth\User;


class AssetRequestFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::all()->random()->id,
            'type' =>  $this->faker->randomElement(['Desktop' ,'Laptop','Addons']),
            'reason' => $this->faker->sentence('1'),
            'delivery_address' => $this->faker->Address(),
            'note' => $this->faker->sentence('1'),
            'status' => $this->faker->randomElement(['0','1','2','3','4']),

        ];
    }
}
