
@extends('layouts.dashboard')
@section('content')
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">

                </div>
            </div><!-- .nk-block-head-content -->

        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block nk-block-lg">

        <div class="row g-gs">
            <div class="col-lg-12">
                <div class="card card-bordered h-100">
                    <div class="card-inner">

                        {!! Form::model($asset, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
                        <div class="form-group">
                            <label class="form-label" >Asset Name <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('name',null, ['class' => 'form-control ', 'placeholder'=>'Enter Asset Name','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" > Asset Type <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('type',[''=>'','Laptop'=>'Laptop','Desktop'=>'Desktop','Addons'=>'Addons'] ,null, ['data-parsley-errors-container' => '#status-errors','data-placeholder' => 'Select Type','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="form-label" >Manufacture. <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('manufacture',null, ['class' => 'form-control ', 'placeholder'=>'Enter Manufacture','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Model No. <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('model_no',null, ['class' => 'form-control ', 'placeholder'=>'Enter Model No.','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Serial No. <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('serial_no',null, ['class' => 'form-control ', 'placeholder'=>'Enter Serial No.','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Note <span>*</span></label>
                            <div class="form-control-wrap">

                                {!! Form::textarea('note',null, ['class' => 'form-control ', 'placeholder'=>'Enter Special Notes']) !!}

                            </div>
                        </div>


                        <div class="form-group">
                            <label class="form-label" > Status <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('status',[''=>'','0'=>'Active','1'=>'Inactive'] ,null, ['data-parsley-errors-container' => '#status-errors','data-placeholder' => 'Select Status','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>

                        </div>

                        <div class="form-group">

                            <button type="submit" class=" float-right btn btn-lg btn-primary">Save</button>
                        </div>

                        {!! Form::close() !!}
                        @if ($mode  === 'EDIT')

                            <form action="{{url('dashboard/calendars/'.$asset->id.'/')}}" method="post">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure?')"   class="btn btn-link" type="submit">Delete Category</button>
                            </form>
                        @else

                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
        </div>
    </div><!-- .nk-block -->


@endsection
