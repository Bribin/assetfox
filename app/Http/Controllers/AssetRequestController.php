<?php

namespace App\Http\Controllers;

use App\Models\AssetRequest;
use App\Models\RequestAllocation;
use Illuminate\Http\Request;

class AssetRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assetrequests =  AssetRequest::whereIn('status',[1,2,3,4])->orderBy('created_at','DESC')->paginate(15);

        $data = [
            'page_title' => 'Approve Requests'
        ];

        return view('dashboard.asset-requests.index',compact('assetrequests'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AssetRequest  $assetRequest
     * @return \Illuminate\Http\Response
     */
    public function show(AssetRequest $assetRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AssetRequest  $assetRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(AssetRequest $assetRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AssetRequest  $assetRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssetRequest $assetRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AssetRequest  $assetRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssetRequest $assetRequest)
    {
        //
    }


    public function approveRequest( $id)
    {
        $assetRequest = AssetRequest::findOrFail($id);

        $assetRequest->status                        = '2';
        $assetRequest->save();

//        $requestID = $request->get('request_id');
//
//        $assetRequest->status           = '3';
//        $assetRequest->save();
        return redirect('dashboard/asset-requests')->with('success', 'Updated Successfully!');
    }


    public function generatePDF($id)
    {
        $assetRequest = AssetRequest::findOrFail($id);

        $assetRequest->status                        = '3';
        $assetRequest->save();

        $assets = RequestAllocation::where('request_id', '=', $assetRequest->id )->get();

//        $assetRequest->status                        = '2';
//        $assetRequest->save();
//
////        $requestID = $request->get('request_id');
////
////        $assetRequest->status           = '3';
////        $assetRequest->save();
///
///
///
///

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('dashboard.challen',compact('assetRequest','assets'));

        return $pdf->download('Delivery_Challan.pdf');
    }


}
