
@extends('layouts.user-dashboard')
@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title ?? '' }}</h3>
                <div class="nk-block-des text-soft">
                    <p>Enter the details and specification of the asset to be requested</p>
                </div>
            </div><!-- .nk-block-head-content -->

        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block nk-block-lg">

        <div class="row g-gs">
            <div class="col-lg-8">
                <div class="card card-bordered h-100">
                    <div class="card-inner">

                        {!! Form::model('', array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
                        <div class="form-group">
                            <label class="form-label" >Asset Type Required <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('type',[''=>'','0'=>'Laptop','1'=>'Desktop','Accessories'] ,null, ['data-parsley-errors-container' => '#status-errors','data-placeholder' => 'Select Type','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label" >Select Reason for Asset Request <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('reason',[''=>'','0'=>'New Joinee','1'=>'Long Leave Return','2'=>'Require Project Procured','3'=>'Issue with Existing Asset'] ,null, ['data-parsley-errors-container' => '#status-errors','data-placeholder' => 'Select Type','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" > Primary Address for Delivery <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::textarea('delivery_address', $deliveryAddress, ['class' => 'form-control','rows' => 1,'placeholder'=>'Enter Category Description..','required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="form-label" > Special Request <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::textarea('note',null, ['class' => 'form-control','rows' => 1,'placeholder'=>'Enter Category Description..','required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>

                        </div>



                        <div class="form-group">

                            <button type="submit" class=" float-right btn btn-lg btn-primary">Save</button>
                        </div>

                        {!! Form::close() !!}
                        @if ($mode  === 'EDIT')

                            <form action="{{url('dashboard/calendars/'.$asset->id.'/')}}" method="post">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure?')"   class="btn btn-link" type="submit">Delete Category</button>
                            </form>
                        @else

                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card card-bordered">
                    <div class="card-inner">
                        <div class="p-5 text-center">
                            <img src="{{ url('/backend/images/livechat-icon.svg') }}">
                        </div>
                        <h4>We’re here to help you!</h4>
                        <p>Ask a question or file a support ticket  or report an issues. Our team support team will get back to you by email.</p>
                        <a href="{{ url('/support-center') }}" class="btn btn-lg btn-outline-primary">Get Support Now</a>
                    </div><!-- .card-inner -->
                </div><!-- .card -->
            </div>
        </div>
    </div><!-- .nk-block -->



@endsection
