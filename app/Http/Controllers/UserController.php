<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users =  User::orderBy('created_at','DESC')->paginate(15);
        $data = [
            'page_title' => 'Manage Users'
        ];

        return view('dashboard.users.index',compact('users'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', $id)->firstOrFail();


        $data = [
            'user' => $user,
            'formMethod' => 'PUT',
            'mode' => 'edit',
            'url' => 'dashboard/users/'.$id,
            'page_title' => ' Edit User # '.$user->name
        ];

        return view('dashboard.users.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->department                    = $request->get('department');
        $user->designation                   = $request->get('designation');
        $user->status                        = $request->get('status');
        $user->role                          = $request->get('role');
        $user->save();

        return redirect('dashboard/users/'.$user->id.'/edit')->with('success', 'Post Updated Successfully!');
    }







    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function profileUpdate(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->address_1                    = $request->get('address_1');
        $user->address_2                    = $request->get('address_2');
        $user->landmark                     = $request->get('landmark');
        $user->district                     = $request->get('district');
        $user->state                        = $request->get('state');
        $user->pincode                      = $request->get('pincode');
        $user->save();

        return redirect('/manage-profile')->with('success', 'Address Updated Successfully!');
    }


    public function addRequest(Request $request)
    {
        return $request->all();
    }

}
