
@extends('layouts.dashboard')
@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title  }}</h3>
                <div class="nk-block-des text-soft">
                    <p>You have total {{ $users->count() }} Assets.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                            <a class="btn btn-sm btn-primary" href="{{ url('dashboard/allocated-list') }}">View Allocated List</a>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="nk-tb-list is-separate mb-3">
            <div class="nk-tb-item nk-tb-head">

                <div class="nk-tb-col"><span class="sub-text">Type</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Status</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Level</span></div>
            </div><!-- .nk-tb-item -->
            @if(count($users ) > 0)
                @foreach($users as $user)
                    <div class="nk-tb-item">
                        <div class="nk-tb-col tb-col-mb">
                            <span >{{ $user->name }} </span>
                        </div>

                        <div class="nk-tb-col">
                            <a href="{{ url('/dashboard/users/'.$user->id.'/edit') }}">
                                <span class="tb-lead">   {{ $user->getLevel() }} </span>
                            </a>
                        </div>

                        <div class="nk-tb-col tb-col-md">
                            <span class="tb-lead">   {{ $user->getRole() }} </span>
                        </div>

                        <div class="nk-tb-col tb-col-lg">
                            <span></span>

                        </div>



                    </div><!-- .nk-tb-item -->

                @endforeach
            @else

            @endif


        </div><!-- .nk-tb-list -->
        <div class="card">
            <div class="card-inner">
                <div class="nk-block-between-md g-3">
                    {{ $users->links() }}

                </div><!-- .nk-block-between -->
            </div><!-- .card-inner -->
        </div><!-- .card -->
    </div>


@endsection
