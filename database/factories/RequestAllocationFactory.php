<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Auth\User;

use App\Models\AssetRequest;
use App\Models\Asset;
class RequestAllocationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'request_id' => AssetRequest::all()->random()->id,
            'asset_id' => Asset::all()->random()->id,
            'delivery_type' => 'DHL',
            'remarks' => '',
            'status' => '1',
        ];
    }
}
