
@extends('layouts.dashboard')
@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title  }}</h3>
                <div class="nk-block-des text-soft">
                    <p>You have total {{ $assetrequests->count() }} Assets.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>

                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="nk-tb-list is-separate mb-3">
            <div class="nk-tb-item nk-tb-head">

                <div class="nk-tb-col"><span class="sub-text">User</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Delivery Assets</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Assets Assigned</span></div>
                <div class="nk-tb-col tb-col-md"><span class="sub-text">Actions</span></div>

            </div><!-- .nk-tb-item -->
            @if(count($assetrequests ) > 0)
                @foreach($assetrequests as $asset)
                    <div class="nk-tb-item">
                        <div class="nk-tb-col tb-col-mb">

                            <span >{{ DB::table('users')->where('id', '=', $asset->user_id )->value('name')  }} </span>
                            <br>
                            <o>{{ $asset->delivery_address }}</o>
                        </div>
                        <div class="nk-tb-col tb-col-mb">

                                @php
                                 $assetList =  DB::table('request_allocations')->where('request_id', '=', $asset->id )->pluck('asset_id')
                                @endphp

                                    @foreach($assetList as $assetID)
                                        <tr>
                                            <td><span class="badge badge-outline-primary">{{ DB::table('assets')->where('id', '=', $assetID )->value('name') }}</span> </td>
                                        </tr>
                                    @endforeach

                        </div>
                        <div class="nk-tb-col tb-col-mb">
                            <span >{{ $asset->status() }} </span>
                        </div>
                        <div class="nk-tb-col tb-col-lg">
                            <div class="drodown">
                                <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <ul class="link-list-opt no-bdr">
                                        @if( $asset->status === '1')
                                            @csrf
                                            <li><a href="{{ url('dashboard/asset-requests/'.$asset->id.'/approveRequest') }}"><em class="icon ni ni-thumbs-up"></em><span>Approve Request</span></a></li>
                                            <!-- <li><a href="{{ url('dashboard/asset-requests/'.$asset->id.'/rejectRequest') }}"><em class="icon ni ni-trash"></em><span>Reject Request</span></a></li> -->
                                        @elseif( $asset->status === '2')
                                            <li><a href="{{ url('dashboard/asset-requests/'.$asset->id.'/generatePDF') }}"><em class="icon ni ni-file-pdf"></em><span>Generate Delivery Challan</span></a></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="nk-tb-col tb-col-lg">

                        </div>



                    </div><!-- .nk-tb-item -->

                @endforeach
            @else

            @endif


        </div><!-- .nk-tb-list -->
        <div class="card">
            <div class="card-inner">
                <div class="nk-block-between-md g-3">
                    {{ $assetrequests->links() }}

                </div><!-- .nk-block-between -->
            </div><!-- .card-inner -->
        </div><!-- .card -->
    </div>


@endsection
