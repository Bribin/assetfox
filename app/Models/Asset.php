<?php

namespace App\Models;
use \Venturecraft\Revisionable\RevisionableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    use RevisionableTrait;
    use HasFactory;

    protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been ma

    public function requestAllocation()
    {
        return $this->belongsToMany(AssetRequest::Class,'request_allocations','request_id');
    }
}
