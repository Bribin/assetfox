
@extends('layouts.dashboard')
@section('content')

    <div class="row g-gs">
        <div class="col-xl-12">
            <div class="card card-bordered">
                <div class="card-inner">
                    <div class="card-title-group pb-3 g-2">
                        <div class="card-title card-title-sm">
                            <h6 class="title">Users Overview</h6>
                            <p>Overall User Status</p>
                        </div>

                    </div>
                    <div class="analytic-ov">
                        <div class="analytic-data-group analytic-ov-group g-3">


                            <div class="analytic-data analytic-ov-data">
                                <div class="title">Total Users</div>
                                <div class="amount"> {{ DB::table('users')->count() }}</div>

                            </div>
                            <div class="analytic-data analytic-ov-data">
                                <div class="title">Employees</div>
                                <div class="amount"> {{ DB::table('users')->where('role','0')->count() }}</div>

                            </div>
                            <div class="analytic-data analytic-ov-data">
                                <div class="title">System Admin</div>
                                <div class="amount">{{ DB::table('users')->where('role','1')->count() }}</div>

                            </div>
                            <div class="analytic-data analytic-ov-data">
                                <div class="title">Adminstrators</div>
                                <div class="amount">{{ DB::table('users')->where('role','2')->count() }}</div>

                            </div>
                            <div class="analytic-data analytic-ov-data">
                                <div class="title">Approved Users</div>
                                <div class="amount">{{ DB::table('users')->where('status','1')->count() }}</div>

                            </div>
                            <div class="analytic-data analytic-ov-data">
                                <div class="title">Users Pending Approval</div>
                                <div class="amount">{{ DB::table('users')->where('status','0')->count() }}</div>

                            </div>
                        </div>


                    </div>
                </div>
            </div><!-- .card -->
        </div><!-- .col -->
        <div class="col-xl-12">
            <div class="card card-bordered">
                <div class="card-inner">
                    <div class="card-title-group pb-3 g-2">
                        <div class="card-title card-title-sm">
                            <h6 class="title">Assets Overview</h6>
                            <p>Overall Asset Status</p>
                        </div>

                    </div>
                    <div class="analytic-ov">
                        <div class="analytic-data-group analytic-ov-group g-3">


                            <div class="analytic-data analytic-ov-data">
                                <div class="title">Total Assets</div>
                                <div class="amount"> {{ DB::table('assets')->count() }}</div>

                            </div>
                            <div class="analytic-data analytic-ov-data">
                                <div class="title">Total Request Assets</div>
                                <div class="amount">{{ DB::table('asset_requests')->count() }}</div>

                            </div>
                            <div class="analytic-data analytic-ov-data">
                                <div class="title">Pending Assets Requests</div>
                                <div class="amount">{{ DB::table('asset_requests')->where('status','0')->count() }}</div>

                            </div>
                            <div class="analytic-data analytic-ov-data">
                                <div class="title">Requests Under Verification</div>
                                <div class="amount">{{ DB::table('asset_requests')->where('status','1')->count() }}</div>

                            </div>
                            <div class="analytic-data analytic-ov-data">
                                <div class="title">Requests Approved</div>
                                <div class="amount">{{ DB::table('asset_requests')->where('status','2')->count() }}</div>

                            </div>
                            <div class="analytic-data analytic-ov-data">
                                <div class="title">Asset Out of Delivery</div>
                                <div class="amount">{{ DB::table('asset_requests')->where('status','3')->count() }}</div>

                            </div>
                            <div class="analytic-data analytic-ov-data">
                                <div class="title">Assets Develivered</div>
                                <div class="amount">{{ DB::table('asset_requests')->where('status','4')->count() }}</div>

                            </div>
                        </div>


                    </div>
                </div>
            </div><!-- .card -->
        </div><!-- .col -->

    </div><!-- .row -->

@endsection
