<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(30)->create();
        \App\Models\Asset::factory(40)->create();
        \App\Models\AssetRequest::factory(40)->create();
        \App\Models\RequestAllocation::factory(100)->create();
    }
}
