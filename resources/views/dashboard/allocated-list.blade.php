
@extends('layouts.dashboard')
@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">Allocated List</h3>
                <div class="nk-block-des text-soft">
                    <p>You have total {{ $allocatedLists->count() }} Asset Allocations.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">

                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="nk-tb-list is-separate mb-3">
            <div class="nk-tb-item nk-tb-head">

                <div class="nk-tb-col"><span class="sub-text">Asset Name</span></div>

                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Allocated User</span></div>

            </div><!-- .nk-tb-item -->
            @if(count($allocatedLists ) > 0)
                @foreach($allocatedLists as $allocatedList)
                    <div class="nk-tb-item">

                        @php
                            $requestID =  DB::table('asset_requests')->where('id', '=', $allocatedList->request_id )->value('user_id');

                        @endphp
                        <div class="nk-tb-col tb-col-mb">
                            <span >{{ DB::table('assets')->where('id', '=', $allocatedList->asset_id )->value('name')  }} </span>
                        </div>


                        <div class="nk-tb-col tb-col-mb">
                            <span >{{ DB::table('users')->where('id', '=', $requestID )->value('name')  }} </span>
                        </div>


                    </div><!-- .nk-tb-item -->

                @endforeach
            @else

            @endif


        </div><!-- .nk-tb-list -->
        <div class="card">
            <div class="card-inner">
                <div class="nk-block-between-md g-3">


                </div><!-- .nk-block-between -->
            </div><!-- .card-inner -->
        </div><!-- .card -->
    </div>


@endsection
