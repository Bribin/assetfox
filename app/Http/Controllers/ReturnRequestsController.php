<?php

namespace App\Http\Controllers;

use App\Models\ReturnRequests;
use Illuminate\Http\Request;

class ReturnRequestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ReturnRequests =  ReturnRequests::orderBy('created_at','DESC')->paginate(15);
        $data = [
            'page_title' => 'Manage Return Requests'
        ];

        return view('dashboard.return-requests.index',compact('ReturnRequests'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReturnRequests  $returnRequests
     * @return \Illuminate\Http\Response
     */
    public function show(ReturnRequests $returnRequests)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReturnRequests  $returnRequests
     * @return \Illuminate\Http\Response
     */
    public function edit(ReturnRequests $returnRequests)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ReturnRequests  $returnRequests
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReturnRequests $returnRequests)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReturnRequests  $returnRequests
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReturnRequests $returnRequests)
    {
        //
    }



    public function AcceptReturn($id)
    {


        $ReturnRequests = ReturnRequests::findOrFail($id);

        $ReturnRequests->status     = 'Approved';
        $ReturnRequests->save();

        return redirect('dashboard/return-requests')->with('success', 'Updated Successfully!');
    }
}
