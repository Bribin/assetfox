<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use Illuminate\Http\Request;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assets =  Asset::orderBy('created_at','DESC')->paginate(15);
        $data = [
            'page_title' => 'Manage Assets'
        ];

        return view('dashboard.assets.index',compact('assets'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'asset' => null,
            'formMethod' => 'POST',
            'mode' => 'CREATE',
            'url' => 'dashboard/assets',
            'page_title' => 'Add a New Asset'
        ];

        return view('dashboard.assets.edit',$data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $asset = new Asset();
        $asset->name                    = $request->get('name');
        $asset->tag                     = strtoupper(\Str::random(8));
        $asset->type                    = $request->get('type');
        $asset->manufacture             = $request->get('manufacture');
        $asset->serial_no               = $request->get('serial_no');
        $asset->model_no                = $request->get('model_no');
        $asset->image                   = '1';
        $asset->warranty                = $request->get('warranty') ?? '2024-11-30';
        $asset->note                    = $request->get('note') ??  'Note';
        $asset->status                  = $request->get('status');
        $asset->save();
        return redirect('dashboard/assets/'.$asset->id.'/edit')->with('success', 'New Asset Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asset = Asset::where('id', $id)->firstOrFail();


        $data = [
            'asset' => $asset,
            'formMethod' => 'PUT',
            'mode' => 'edit',
            'url' => 'dashboard/assets/'.$id,
            'page_title' => ' Edit '.$asset->title
        ];

        return view('dashboard.assets.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $asset = Asset::findOrFail($id);
        $asset->name                    = $request->get('name');
        $asset->type                    = $request->get('type');
        $asset->manufacture             = $request->get('manufacture');
        $asset->serial_no               = $request->get('serial_no');
        $asset->model_no                = $request->get('model_no');
        $asset->warranty                = $request->get('warranty') ?? '2024-11-30';
        $asset->note                    = $request->get('note') ??  'Note';
        $asset->status                  = $request->get('status');
        $asset->save();

        return redirect('dashboard/assets/'.$asset->id.'/edit')->with('success', 'Asset Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
        //
    }


    public function downloadAssetTag($id)
    {

        $asset = Asset::findOrFail($id);
        return \QrCode::size(250)->generate($asset->tag);
    }
}
