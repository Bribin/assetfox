@extends('layouts.guest')

@section('content')

    <div class="nk-block nk-block-middle nk-auth-body  wide-xs ">
        <div class="card card-bordered">
            <div class="card-inner card-inner-lg">
                <div class="nk-block-head">
                    <div class="nk-block-head-content">
                        <h4 class="nk-block-title">{{ __('Register') }}</h4>
                        <div class="nk-block-des">
                            <p>Welcome to AssetFox </p>
                        </div>
                    </div>
                </div>
                <form method="POST" data-parsley-validate action action="{{ route('register') }}">
                    @csrf
                    <div class="form-group">
                        <div class="form-label-group">
                            <label class="form-label" for="name">{{ __('Name') }}</label>

                        </div>
                        <div class="form-control-wrap">

                            <input id="name" type="text" class="form-control form-control-lg  @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-label-group">
                            <label class="form-label" for="email">{{ __('Email Address') }}</label>

                        </div>
                        <div class="form-control-wrap">
                            <input id="email" type="text" class="form-control form-control-lg  @error('name') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-label-group">
                            <label class="form-label" for="password">{{ __('Password') }}</label>

                        </div>
                        <div class="form-control-wrap">

                            <input id="password" type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" autocomplete="new-password" required autocomplete="email">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-label-group">
                            <label class="form-label" for="password-confirm">{{ __('Confirm Password') }}</label>

                        </div>
                        <div class="form-control-wrap">

                            <input id="password-confirm" type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" name="password_confirmation" value="{{ old('password_confirmation') }}" autocomplete="new-password" required autocomplete="email">


                        </div>
                    </div>
                    <div class="form-group">

                        <button type="submit" class="btn btn-lg btn-primary btn-block">
                            {{ __('Register') }}
                        </button>
                    </div>
                </form>
                <div class="form-note-s2 text-center pt-4"> Already have an account? <a href="{{ url('/login') }}">Sign in</a>
                </div>

            </div>
        </div>
    </div>
@endsection
