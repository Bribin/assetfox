
@extends('layouts.dashboard')
@section('content')
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">

                </div>
            </div><!-- .nk-block-head-content -->

        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block nk-block-lg">

        <div class="row g-gs">
            <div class="col-lg-12">
                <div class="card card-bordered h-100">
                    <div class="card-inner">
                    @php
                        $usrName =  DB::table('users')->where('id', '=', $ticket->user_id )->value('name');
                    @endphp
                        <p>TICKET ID : {{ $ticket->id  }}</p>
                        <span >TICKET USER : {{ $usrName }} </span>

                        <p>TICKET TITLE :{{ $ticket->title  }}</p>
                        <p>TICKET NOTE : {{ $ticket->note  }}</p>


                        {!! Form::model($ticket, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
                        <div class="form-group">
                            <label class="form-label" >Reply Message <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::textarea('content',null, ['class' => 'form-control ', 'placeholder'=>'Enter Asset Name','required' =>'required']) !!}
                            </div>
                        </div>


                        <div class="form-group">

                            <button type="submit" class=" float-right btn btn-lg btn-primary">Save</button>
                        </div>

                        {!! Form::close() !!}
                        @if ($mode  === 'EDIT')

                            <form action="{{url('dashboard/calendars/'.$asset->id.'/')}}" method="post">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure?')"   class="btn btn-link" type="submit">Delete Category</button>
                            </form>
                        @else

                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
        </div>
    </div><!-- .nk-block -->


@endsection
