
@extends('layouts.user-dashboard')
@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">Support Messages</h3>

            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">

                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">

        <div class="card">
            <div class="card-inner">
                <div class="nk-block-between-md g-3">

                            @php
                            $messages = DB::table('ticket_messages')->where('user_id', '=', Auth::user()->id)->get()
                            @endphp


                    <div class="nk-tb-list is-separate mb-3">
                        <div class="nk-tb-item nk-tb-head">

                            <div class="nk-tb-col"><span class="sub-text">Content</span></div>
                            <div class="nk-tb-col tb-col-mb"><span class="sub-text">Created At</span></div>


                        </div><!-- .nk-tb-item -->
                        @if(count($messages ) > 0)
                            @foreach($messages as $message)
                                <div class="nk-tb-item">
                                        <div class="nk-tb-col tb-col-mb">
                                          {{ $message->content }}
                                        </div>

                                        <div class="nk-tb-col tb-col-mb">
                                            <span >{{ $message->created_at }} </span>
                                        </div>




                                </div><!-- .nk-tb-item -->

                            @endforeach
                        @else

                        @endif


                    </div><!-- .nk-tb-list -->

                </div><!-- .nk-block-between -->
            </div><!-- .card-inner -->
        </div><!-- .card -->
    </div>


@endsection
