
@extends('layouts.dashboard')
@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title  }}</h3>
                <div class="nk-block-des text-soft">
                    <p>You have total {{ $ReturnRequests->count() }} Assets.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>

                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="nk-tb-list is-separate mb-3">
            <div class="nk-tb-item nk-tb-head">

                <div class="nk-tb-col"><span class="sub-text">User</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Assets</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Reason</span></div>
                <div class="nk-tb-col tb-col-md"><span class="sub-text">Actions</span></div>

            </div><!-- .nk-tb-item -->
            @if(count($ReturnRequests ) > 0)
                @foreach($ReturnRequests as $ReturnRequest)
                    <div class="nk-tb-item">
                        <div class="nk-tb-col tb-col-mb">

                            <span >{{ DB::table('users')->where('id', '=', $ReturnRequest->user_id )->value('name')  }} </span>


                        </div>
                        <div class="nk-tb-col tb-col-mb">
                            <span >{{ DB::table('assets')->where('id', '=', $ReturnRequest->asset_id )->value('name')  }} </span>

                        </div>
                        <div class="nk-tb-col tb-col-mb">
                            <span > {{ $ReturnRequest->note }}</span>
                        </div>
                        <div class="nk-tb-col tb-col-lg">
                          @if($ReturnRequest->status == 'Approved')
                                <p>Approved Request</p>
                          @else

                                <a href="{{ url('dashboard/return-requests/'.$ReturnRequest->id.'/acceptReturn') }}">Accept Return Request</a>
                            @endif
                        </div>

                        <div class="nk-tb-col tb-col-lg">

                        </div>



                    </div><!-- .nk-tb-item -->

                @endforeach
            @else

            @endif


        </div><!-- .nk-tb-list -->
        <div class="card">
            <div class="card-inner">
                <div class="nk-block-between-md g-3">
                    {{ $ReturnRequests->links() }}

                </div><!-- .nk-block-between -->
            </div><!-- .card-inner -->
        </div><!-- .card -->
    </div>


@endsection
