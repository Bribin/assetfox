<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

class AssetRequest extends Model
{

    use RevisionableTrait;
    use HasFactory;

    protected $revisionEnabled = true;
    protected $historyLimit = 500; //Stop tracking revisions after 500 changes have been ma

    protected $table = 'asset_requests';


    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public  function status(){

        $role= $this->status;
        switch ($role) {
            case "0":
                echo '<span class="badge badge-default">PENDING</span>';
                break;
            case "1":
                echo '<span class="badge badge-warning">UNDER VERIFICATION</span>';
                break;
            case "2":
                echo '<span class="badge badge-success"> APPROVED</span>';
                break;
            case "3":
                echo '<span class="badge badge-danger"> OUT FOR DELIVERY</span>';
                break;
            case "4":
                echo '<span class="badge badge-warning"> DELIVERY CONFIRMED</span>';
                break;

        }




    }

    public function assets()
    {
        return $this->belongsToMany(Asset::class,'request_allocations','asset_id')->withTimestamps();
    }




    public  function requestType(){

        $role= $this->type;
        switch ($role) {
            case "0":
                echo '<span class="badge badge-outline-default">LAPTOP</span>';
                break;
            case "1":
                echo '<span class="badge badge-outline-default">DESKTOP</span>';
                break;
            case "2":
                echo '<span class="badge badge-outline-default"> ADDONS</span>';
                break;


        }

    }

    public  function requestReason(){

        $role= $this->type;
        switch ($role) {
            case "0":
                echo '<span class="badge badge-success">New Joinee</span>';
                break;
            case "1":
                echo '<span class="badge badge-success">Long Leave Return</span>';
                break;
            case "2":
                echo '<span class="badge badge-success">Issue with Existing Asset</span>';
                break;

        }

    }



}
