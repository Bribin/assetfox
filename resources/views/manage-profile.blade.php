
@extends('layouts.user-dashboard')
@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">
                    <p>Enter your address to which the assets are to be delivered</p>
                </div>
            </div><!-- .nk-block-head-content -->

        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block nk-block-lg">

        <div class="row g-gs">
            <div class="col-lg-4">
                <div class="card card-bordered">
                    <div class="card-inner">
                        <div class="team">

                            <div class="user-card user-card-s2">
                                <div class="user-avatar lg bg-primary">
                                    <span>AB</span>
                                    <div class="status dot dot-lg dot-success"></div>
                                </div>
                                <div class="user-info">
                                    <h6>{{ $profile->name }}</h6>
                                    <span class="sub-text">{{ $profile->level  }}</span>
                                    <span class="sub-text">{{ $profile->department  }}</span>
                                    <span class="sub-text">{{ $profile->designation  }}</span>
                                </div>
                            </div>
                            <ul class="team-info">
                                <li><span>Email</span><span>{{ $profile->email }}</span></li>
                                <!-- <li><span>Phone</span><span>{{ $profile->phone }}</span></li> -->
                            </ul>

                        </div><!-- .team -->
                    </div><!-- .card-inner -->
                </div><!-- .card -->
            </div>
            <div class="col-lg-8">

                <div class="card card-bordered h-100">
                    <div class="card-inner">

                        {!! Form::model($profile, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
                        <div class="form-group">
                            <label class="form-label" >Address Line 01 <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('address_1',null, ['class' => 'form-control ', 'placeholder'=>'Enter Address Line 01', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label" >Address Line 02 <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('address_2',null, ['class' => 'form-control ', 'placeholder'=>'Enter Asset Name', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label" >Land Mark <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('landmark',null, ['class' => 'form-control ', 'placeholder'=>'Enter Asset Name', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">District <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('district',null, ['class' => 'form-control ', 'placeholder'=>'Enter DIs', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="form-label">State<span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('state',null, ['class' => 'form-control ', 'placeholder'=>'Enter State', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="form-label">Special Request <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('pincode',null, ['class' => 'form-control ', 'placeholder'=>'Enter Pincode', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>

                        </div>
                        <div class="form-group">

                            <button type="submit" class=" float-right btn btn-lg btn-primary">Save</button>
                        </div>
                        {!! Form::close() !!}
                        @if ($mode  === 'EDIT')

                            <form action="{{url('dashboard/calendars/'.$asset->id.'/')}}" method="post">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure?')"   class="btn btn-link" type="submit">Delete Category</button>
                            </form>
                        @else

                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div><!-- .nk-block -->



@endsection
