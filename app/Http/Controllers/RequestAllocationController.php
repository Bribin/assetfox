<?php

namespace App\Http\Controllers;

use App\Models\RequestAllocation;
use Illuminate\Http\Request;

class RequestAllocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RequestAllocation  $requestAllocation
     * @return \Illuminate\Http\Response
     */
    public function show(RequestAllocation $requestAllocation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RequestAllocation  $requestAllocation
     * @return \Illuminate\Http\Response
     */
    public function edit(RequestAllocation $requestAllocation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RequestAllocation  $requestAllocation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequestAllocation $requestAllocation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RequestAllocation  $requestAllocation
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequestAllocation $requestAllocation)
    {
        //
    }
}
