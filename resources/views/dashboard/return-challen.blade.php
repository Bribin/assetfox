
<table class="GeneratedTable">
    <thead>
    <tr>
{{ $assets }}
        <td>
           <p> Delivery Partner</p>
            <?php
            $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
            echo $generator->getBarcode('081231723897', $generator::TYPE_CODE_39);
            echo '081231723897';
            ?>

        </td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td >
            <h4>Ship From</h4>
            <p>Konni Ventures Pvt Ltd <br>
                First Floor, Geo Infopark,<br>
                Infopark Campus, Kochi,<br>
                Kerala,India 682042<br>
            </p></td>

        <td>
            <h4>Delivery Address</h4>
            <p>{{ DB::table('users')->where('id', '=', $assetRequest->user_id )->value('name') }} </p>
            <p>{{ $assetRequest->delivery_address }}
            </p>
        </td>

    </tr>
    <tr>
        <td><h4>Package Detials</h4>

                @foreach($assets as $assetID)
                       <p>     {{ DB::table('assets')->where('id', '=', $assetID->asset_id )->value('name') }}</p>
                @endforeach
            <p>Date : {{ $assetRequest->updated_at }}   <br>
            </p>

            <p>{{ $assetRequest->remarks }}</p>
        </td>

    </tr>
    <tr>
        <?php
        $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
        $random = strtoupper(\Str::random(12));
        echo $generator->getBarcode($random, $generator::TYPE_CODE_39);
        echo $random;
        ?>

    </tr>
    </tbody>
</table>
<p>  If  Package is not delivered please return to Konni Ventures Pvt Ltd, First Floor, Geo Infopark, Infopark Campus, Kochi, Kerala,India 682030</p>
<!-- Codes by Quackit.com -->



