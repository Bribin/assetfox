<?php

use Illuminate\Support\Facades\Route;
use App\Models\AssetRequest;
use App\Models\ReturnRequests;
use App\Models\Asset;
use App\Models\Tickets;
use App\Models\Ticket_messages;
use App\Models\User;
use App\Models\RequestAllocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//============= EMPLOYEE ROUTES ===========================//


Route::get('/text', function () {

    $assetRequests =    AssetRequest::findOrFail('2');

    $data = \App\Models\RequestAllocation::where('request_id',$assetRequests->id)->get();


    return $data;


//    $request->assets()->sync($request->input('assetsList'));
//    $request->status                 = '1';
//    $request->save();




});




    Auth::routes();





Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
        return view('index');
    });

    Route::get('/manage-profile', function () {
        $profile = \Illuminate\Support\Facades\Auth::user();
        $data = [
            'profile' => $profile,
            'formMethod' => 'POST',
            'mode' => 'CREATE',
            'url' => 'manage-profile/'.$profile->id.'/profileUpdate',
            'page_title' => 'Manage My Info'
        ];
        return view('manage-profile',compact('profile'),$data );
    });

    Route::get('/manage-assets', function () {
         $userID = \Illuminate\Support\Facades\Auth::user()->id;
         $assetRequests =    AssetRequest::where('user_id',$userID)->get();


         $data = [
            'requests' => $assetRequests,
            'formMethod' => 'PUT',
            'mode' => 'edit',
            'url' => '',
            'page_title' => 'Manage My Assets',

        ];

        return view('assets-list',compact('assetRequests'),$data );

    });

    Route::get('/request-assets', function () {
        $userID = \Illuminate\Support\Facades\Auth::user();
        $deliveryAddress = $userID->address_1.', '.$userID->address_2.', '.$userID->landmark.', '.$userID->district.', '.$userID->state.', '.$userID->pincode.', Contact# ' .$userID->phone;
        $data = [
            'formMethod' => 'POST',
            'mode' => 'edit',
            'url' => '/add-request',
            'page_title' => 'Request a new Request',
        ];

        return view('request-assets',compact('deliveryAddress'),$data);
    });

    Route::get('/support-center', function () {
        $data = [
            'ticket'=>'Null',
            'formMethod' => 'POST',
            'mode' => 'edit',
            'url' => '/create-ticket',
            'page_title' => 'Request a new Request',
        ];
        return view('support-center',$data);
    });



    Route::post('/manage-profile/{id}/profileUpdate', function (Request $request, $id) {
        $user = User::findOrFail($id);
        $user->address_1                    = $request->get('address_1');
        $user->address_2                    = $request->get('address_2');
        $user->landmark                     = $request->get('landmark');
        $user->district                     = $request->get('district');
        $user->state                        = $request->get('state');
        $user->pincode                      = $request->get('pincode');
        $user->save();

        return redirect('/manage-profile')->with('success', 'Address Updated Successfully!');
    });

    Route::post('/create-ticket', function (Request $request) {



        $ticket = new Tickets();
        $ticket->user_id              = \Illuminate\Support\Facades\Auth::user()->id;
        $ticket->ticket_type              = '1';
        $ticket->category              = $request->get('category');
        $ticket->priority            = $request->get('priority');
        $ticket->title              = $request->get('title');
        $ticket->note              = $request->get('note');
        $ticket->status            = '0';
        $ticket->save();

        return redirect('/')->with('success', 'Support Ticket Created Successfully!');

    });

    Route::post('/add-request', function (Request $request) {
        $AssetRequest = new AssetRequest();
        $AssetRequest->user_id              = \Illuminate\Support\Facades\Auth::user()->id;
        $AssetRequest->type              = $request->get('type');
        $AssetRequest->reason            = $request->get('reason');
        $AssetRequest->delivery_address  = $request->get('delivery_address');
        $AssetRequest->note              = $request->get('note');
        $AssetRequest->status            = '0';
        $AssetRequest->save();

        return redirect('manage-assets')->with('success', 'Asset Created Successfully!');

    });


    Route::get('manage-assets/{id}/ConfirmAssetDelivery', function ($id) {

        $assetRequest = AssetRequest::findOrFail($id);

        $assetRequest->status                        = '4';
        $assetRequest->save();

        return redirect('/manage-assets')->with('success', 'Updated Successfully!');
    });

    Route::get('manage-assets/{id}/ReturnRequest', function ($id) {

        $assetID = $id;
        $data = [
            'formMethod' => 'POST',
            'mode' => 'edit',
            'url' => 'returnRequest',
            'page_title' => 'Request a new Request',
        ];



        return view('return-request',compact('assetID'),$data);
    });


    Route::post('/returnRequest', function (Request $request) {

        $ReturnRequest = new ReturnRequests();
        $ReturnRequest->user_id           = \Illuminate\Support\Facades\Auth::user()->id;
        $ReturnRequest->asset_id          = $request->get('asset_id');
        $ReturnRequest->note              = $request->get('note');
        $ReturnRequest->status            = 'Pending';
        $ReturnRequest->save();

        return redirect('manage-assets')->with('success', 'Asset Created Successfully!');

    });


    Route::get('/inbox', function () {

        return view('inbox');
    });


});

//============= SYSTEM ADMIN ROUTES ===========================//

Route::group(['middleware'=>['IsSystemAdminMiddleware','auth']], function(){
    Route::get('/system-admin', function () {

        $requests =  AssetRequest::where('status' ,'0')->paginate(15);
        $data = [
            'requests' => $requests,
            'formMethod' => 'POST',
            'mode' => 'CREATE',
            'url' => 'dashboard/profile',
            'page_title' => 'View All Request'
        ];
        return view('system-admin.request-list',compact('requests'),$data );

    });

    Route::get('/system-admin/view-request/{id}', function ($id) {

        $request = AssetRequest::where('id', $id)->firstOrFail();
        $allocatedAssets = RequestAllocation::pluck('asset_id')->toArray();
        $assetsList =  Asset::pluck('name','id')->except($allocatedAssets);
        return view('system-admin.view-request',compact('request','assetsList'));
    });

    Route::post('/system-admin/assign-assets/', function ( Request $request) {

        $requestID = $request->get('request_id');

        $assets = $request->get('assetsList');


        foreach ($assets as $assetID) {
            $RequestAllocation = new RequestAllocation();
            $RequestAllocation->asset_id             = $assetID;
            $RequestAllocation->request_id           = $requestID;
            $RequestAllocation->delivery_type        = $request->get('delivery_type');
            $RequestAllocation->remarks              = $request->get('remarks');
            $RequestAllocation->status               = '0';
            $RequestAllocation->save();
        }

        $assetRequest =  AssetRequest::findOrFail($requestID);

        $assetRequest->status                        = '1';
        $assetRequest->save();




        return redirect('system-admin')->with('success', 'Asset Created Successfully!');
    });

});





//============= ADMIN ROUTES ===========================//


Route::group(['middleware' => ['isAdmin','auth']], function() {

    Route::get('/dashboard', function () {
        return view('dashboard.index');

    });

    Route::resource('dashboard/tickets', \App\Http\Controllers\TicketsController::class);
    Route::resource('dashboard/assets', \App\Http\Controllers\AssetController::class);
    Route::resource('dashboard/users', \App\Http\Controllers\UserController::class);
    Route::resource('dashboard/asset-requests', \App\Http\Controllers\AssetRequestController::class);
    Route::resource('dashboard/return-requests', \App\Http\Controllers\ReturnRequestsController::class);

    Route::post('dashboard/users/{id}/profileUpdate', [\App\Http\Controllers\UserController::class, 'profileUpdate']);

    Route::get('dashboard/asset-requests/{id}/approveRequest', [\App\Http\Controllers\AssetRequestController::class, 'approveRequest']);
    Route::get('dashboard/asset-requests/{id}/generatePDF', [\App\Http\Controllers\AssetRequestController::class, 'generatePDF']);

    Route::get('dashboard/assets/{id}/downloadAssetTag', [\App\Http\Controllers\AssetController::class, 'downloadAssetTag']);

    Route::get('/dashboard/asset-log', function () {
        $systemLogs = \Venturecraft\Revisionable\Revision::paginate('30');
        return view('dashboard.system-log',compact('systemLogs'));
    });

    Route::get('/dashboard/return-requests/{id}/acceptReturn', [\App\Http\Controllers\ReturnRequestsController::class, 'AcceptReturn']);


    Route::get('/dashboard/tickets/{id}/reply', function ($id) {
            $ticket =  Tickets::findOrFail($id);

            $data = [
                'ticket'=>  $ticket,
                'formMethod' => 'POST',
                'mode' => 'edit',
                'url' => '/dashboard/tickets/'.$ticket->id.'/replyticket',
                'page_title' => 'Request a new Request',
            ];
            return view('dashboard.tickets.reply',$data);
        });

    Route::post('/dashboard/tickets/{id}/replyticket', function (Request $request, $id) {

            $ticket =  Tickets::findOrFail($id);

            $ticketMsg =  new Ticket_messages();

            $ticketMsg->ticket_id            = $ticket->id;
            $ticketMsg->user_id              = $ticket->user_id;
            $ticketMsg->content              = $request->get('content');
            $ticketMsg->save();

            return redirect('dashboard/tickets/')->with('success', 'Updated Successfully!');
        });



    Route::get('/dashboard/allocated-list', function () {

        $allocatedLists = RequestAllocation::all();

        return view('dashboard.allocated-list',compact('allocatedLists'));
    });


    //Clear View cache:
    Route::get('/view-clear', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('optimize');
    $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('config:cache');
    return '<h1>View cache cleared</h1>';
});

});