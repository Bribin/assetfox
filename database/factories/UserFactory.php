<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'department' => '',
            'Designation' => '',
            'phone' => $this->faker->unique()->phoneNumber(),
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // passwor
            'role' => $this->faker->randomElement(['0' ,'1', '2', '3']),
            'address_1' => $this->faker->Address(),
            'address_2' => $this->faker->secondaryAddress(),
            'landmark' => $this->faker->secondaryAddress(),
            'state' => $this->faker->state(),
            'district' => $this->faker->city(),
            'pincode' => $this->faker->postcode(),
            'status' => $this->faker->randomElement(['0', '1']),
            'remember_token' => Str::random(10),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
