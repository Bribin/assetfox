<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class AssetFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company(),
            'tag' => $this->faker->regexify('[A-Za-z0-9]{20}'),
            'type' =>  $this->faker->randomElement(['Desktop' ,'Laptop','Addons']),
            'manufacture' => $this->faker->randomElement(['Dell' ,'Lenvo','HP']),
            'serial_no' => $this->faker->regexify('[A-Za-z0-9]{20}'),
            'model_no' =>   $this->faker->regexify('[A-Za-z0-9]{20}'),
            'warranty' => $this->faker->creditCardExpirationDate(),
            'image' =>  '',
            'note' =>  '',
            'status' =>  $this->faker->randomElement(['0' ,'1']),
        ];
    }
}
